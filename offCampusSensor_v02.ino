/* ==========================================================
Author: Jason DeCosta
Date: 2/12/2016
This sample code reads data from a sensor on an arduino UNO 
and sends it to the MerMAID server where it is stored in the 
database and displayed on the website. 
===========================================================*/
#include <SPI.h>                      //Serial Peripheral Interface (SPI). This allows us to use the serial monitor. To view go to: tools > serial monitor
#include <Ethernet.h>                 //used to connect arduino to the internet

int photoRPin = 0;                    //pin on arduino which sensor is connected to
int minLight;                         //used to calibrate the light readings
int maxLight;                         //used to calibrate the light readings
int lightLevel;                       //value read from sensor
int adjustedLightLevel;               //light level after setting min and max constraint 
char sensor1[] = "PhotoSensor";       //identifying the Sensor in database table, column = (SENSOR). You could use the room that the sensor is in or its physical location
char boardID[] = "UNO";               //naming the Arduino Board (Could be location of borad...or however you wish to identify it in the database table)
char unitOfMeasurement[] = "Light";   //unit is a column on the table in database used to identify the sensor's unit of measurement
byte ipaddr[] = {192, 168, 1, 12};    //used to request an ip address from your router. No need to change this, you may get a random ip, this is ok
char host[] = "capegreenenergy.com";  //domain url of the server
char url_insertPHP [] = "/mermaid/jason/insert_data_itsquad.php"; //path to insert.php used to insert data into database. Use separate URL for each sensor
boolean printSwitch;                  //switch used to determine what messages will be printed to serial monitor. 
IPAddress server(76, 118, 144, 24);   //MerMAID Server IP, check to make sure IP has not changed (ping the website to find IP, EX: ping www.capegreenenergy.com
EthernetClient client;                //create ethernet client object, named client

void setup(){                         //setting up light sensor parameters and connecting to router 
  Serial.begin(9600);                 //start serial monitor. To view go to: tools > serial monitor   
  lightLevel=analogRead(photoRPin);   //setup the starting light level by reading photo sensor
  minLight=lightLevel-20;             //minus 20 from original light reading and use that as 0 (scale will be 0-100)
  maxLight=lightLevel;                //original light reading will be used as max (100)
  Ethernet.begin(ipaddr);             //request ip from router
  delay(1000);                        //delay 1 second to allow time to receive ip address and connect to your router
}                                     //end setup

void loop(){                          //main loop runs all seb functions
  readMySensor();                     //function that reads the sensor
  sendData();                         //function which sends sensor data to server
  printToSerialMonitor();             //function that checks connection status and prints messages accordingly  
  delay(1000);                        //set loop interval here. This will run the loop once per second. 
}                                     //end main loop() function

void printToSerialMonitor(){                      //function that checks connection status and prints messages accordingly 
  if (printSwitch == true){                       //if connection to server was successful do the following, printSwitch is set to true in function sendData
    Serial.println("Connected ... sending data ..."); //printing message to serial monitor
    Serial.print("Curent Value = ");              //printing message to serial monitor
    Serial.println(adjustedLightLevel);           //send the adjusted Light level result to Serial monitor
    Serial.println("Light Value has been sent!"); //printing message to serial monitor
    Serial.println();                             //print blank line 
  }else if(printSwitch == false){                 //if the arduino failed to connect to server
    Serial.println("COULD NOT CONNECT!!");
    Serial.println();                             //print blank line
  }                                               //end else if statement
}                                                 //end printToSerialMonitor function

void sendData(){                                  //function connects to the server and sends data to the database
  if (client.connect(server, 80)){                //if established connection to the server, using port 80 for http    
      printSwitch = true;                         //set printSwitch to true, print connected message, used in printToSerialMonitor()
      client.print ("GET ");                      //this is an http GET request
      client.print (url_insertPHP );              //path to insert php file on server
      client.print ("?Value=");                   //variable "Value" must be same as variable used in PHP code, which is "Value"
      client.print (adjustedLightLevel);          //last read from the sensor
      client.print ("&Unit=");                    //variable "Unit" must be same as variable used in PHP code, which is "Unit"   
      client.print (unitOfMeasurement);           //dont know what this is, possibly database unit of measurement
      client.print ("&Sensor=");                  //variable "Sensor" must be same as variable used in PHP code, which is "Sensor"
      client.print (sensor1);                     //You defined this at the start of your code
      client.print ("&BoardID=");                 //variable "BoardID" must be same as variable used in PHP code, which is "BoardID" 
      client.print (boardID);                     //you defined this at the start of your code
      client.println (" HTTP/1.1");               //use http version 1.1
      client.print ("Host: ");                    //the host url will be printed after this line of code
      client.println (host);                      //host domain is capegreenenergy.com
      client.println();                           //this adds a blank line to the packet
      client.stop();                              //close out the current connection to the server
      client.flush();                             //flush currently stored values from current iteration of main loop
  }else{                                          //if arduino failed to connect to the server do the following
      printSwitch = false;                        //set to false (used in printToSerialMonitor() function)
  }                                               //end else if statement
}                                                 //end sendData() function

void readMySensor(){                              //function used to read the sensor, put your sensor code in this function
  lightLevel = analogRead(photoRPin);             //read sensor and store value in variable lightLevel
    if(minLight > lightLevel){                    //if minLight is greater than lightlevel
      minLight = lightLevel;                      //than minLight equals lightLevel
    }                                             //end if statement
    if(maxLight < lightLevel){                    //if maxLight is less than lightLevel
      maxLight = lightLevel;                      //maxLight equals lightLevel
    }                                             //end if statement
  adjustedLightLevel = map(lightLevel, minLight, maxLight, 0, 100); //Adjust the light level to produce a result between 0 and 100.
}                                                 //end readMySensor() function
